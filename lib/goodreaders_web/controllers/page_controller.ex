defmodule GoodreadersWeb.PageController do
  use GoodreadersWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
